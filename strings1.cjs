function convertToNumeric(str) {
    // remove any non-numeric characters from the string
    const numericString = str.replace(/[^0-9.-]+/g, '');
  
    // convert the resulting string to a numeric value
    const numericValue = parseFloat(numericString);
  
    return numericValue;
    if(NaN(numericValue)){
        return 0;
    }+
  }
  module.exports= convertToNumeric;