function getMonthFromDate(dateString) {
  const dateParts = dateString.split("/");
  const month = dateParts[0];
  return month;
}

module.exports = getMonthFromDate;

