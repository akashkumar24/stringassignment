function getIpAddressArray(ipAddress) {
    const ipAddressArray = ipAddress.split(".").map(Number);
    return ipAddressArray;
  }
  
  module.exports = getIpAddressArray;
  