function joinArrayToString(arr) {
    if (arr.length === 0) {
      return '';
    }
    return arr.join(' ') + '.';
  }
module.exports=joinArrayToString;  